{
NIM
-hra 2 hracu, stridaji se na tahu
-hromadka sirek, urcen minimalni a maximalni odber, prohrava ten, ktery bere
 posledni sirku (prip. vyhrava ten, ktery bere posledni)
-algoritmus vyhry: pocet na hromadce minus 1, vzit zbytek po deleni
 minimum + maximum
}

var Pocet, Min, Max, Tah, pcHAZ, ichHAZ : integer;
    NaTahuPC : boolean; {logicky typ, hodnoty false a true}

BEGIN
Randomize;
writeln('hra NIM, vyhrava ten, kdo nesebere posledni sirku');
writeln('zadej pocet sirek');
readln(Pocet);

Min := 1; {zatim 1}

writeln('zadej maximalni pocet k odberu');
readln(Max);

NaTahuPC := false;

repeat
if NaTahuPC then
 Begin
  Tah := (Pocet - 1) mod (Min + Max);
  if Tah = 0 then Tah := Random (Max - Min) + Min;
  writeln('PC odebira sirek: ',Tah);
  inc(pcHAZ, Tah);
 End
            else
 Begin
  repeat
   writeln('zadej pocet sirek, ktere beres');
   readln(Tah);
   inc(ichHAZ, Tah);
  until (Tah >= Min) and (Tah <= Max) and (Tah <= Pocet)

 End;

dec(Pocet,Tah);
writeln('na hromadce zbyva sirek: ', Pocet);
NaTahuPC := not NaTahuPC

until Pocet=0;

if NaTahuPC then writeln('prohral jsi')
            else writeln('gratuluji k vitezstvi');
writeln('Pocitac ma ',pcHAZ,' sirek.');
writeln('Ty mas ',ichHAZ,' sirek.');
readln
END.

ToDo list:
1. Zadani Pocet sirek alespon 1.
2. Zadani minima vc. kontroly.
3. Obtiznost - pro PC - napr. 0 - 3 (0 vzdy nahodne generuje, 3 vzdy vyhraje, kdyz lze vyhrat)
4. Vyber, kdo je na tahu (PC, clovek, generovat).
5. Koho co napadne.
