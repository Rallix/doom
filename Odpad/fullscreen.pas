PROGRAM FullScreen;
USES
    Windows;
BEGIN
keybd_event(VK_MENU,56,0,0);
//VK_MENU is virtual-key code of ALT.
//56 is scan code of ALT.
keybd_event(VK_RETURN,28,0,0);
//VK_RETURN is virtual-key code of ENTER.
//28 is scan code of ENTER.
//Both Alt and Enter are being 'pressed.'
keybd_event(VK_MENU,56,
            KEYEVENTF_KEYUP,0);
//Alt is up.
keybd_event(VK_RETURN,28,
            KEYEVENTF_KEYUP,0);
//Enter is up.
Readln;//Wait for enter.
END.